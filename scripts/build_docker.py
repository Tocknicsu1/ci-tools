#!/usr/bin/env python3
import subprocess
import os
import traceback
import sys

import env


def shell(command, ignore_error=False):
    print('*', command)
    output = b''
    try:
        output = subprocess.check_output(
            ['/usr/bin/env', *command], stderr=subprocess.STDOUT)
    except Exception as e:
        print("Exception in user code:")
        print("-" * 60)
        traceback.print_exc(file=sys.stdout)
        print("-" * 60)
        if not ignore_error:
            raise e
    print(output.decode())


if __name__ == '__main__':
    shell(
        ['docker', 'login', '-u', 'gitlab-ci-token',
         '-p', os.environ['CI_BUILD_TOKEN'], 'registry.gitlab.com'])

    shell(
        ['docker', 'pull', '%s:latest' % (os.environ['CI_IMAGE'],)],
        ignore_error=True
    )

    shell(
        [
            'docker', 'build',
            '--cache-from', '%s:latest' % (os.environ['CI_IMAGE'],),
            '--tag', '%s:%s' % (
                os.environ['CI_IMAGE'],
                os.environ['CI_IMAGE_TAG']),
            '.'
        ]
    )

    shell(
        [
            'docker', 'push', '%s:%s' % (
                os.environ['CI_IMAGE'],
                os.environ['CI_IMAGE_TAG']),
        ]
    )
