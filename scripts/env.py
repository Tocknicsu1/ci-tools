#!/usr/bin/env python3
import os

if 'CI_COMMIT_TAG' in os.environ:
    os.environ['CI_IMAGE'] = '%s/%s/%s' % (
        os.environ['CI_REGISTRY'],
        os.environ['CI_PROJECT_PATH'],
        'master'
    )
    os.environ['CI_IMAGE_TAG'] = os.environ['CI_COMMIT_TAG']
else:
    os.environ['CI_IMAGE'] = '%s/%s/%s' % (
        os.environ['CI_REGISTRY'],
        os.environ['CI_PROJECT_PATH'],
        os.environ['CI_COMMIT_REF_SLUG']
    )
    os.environ['CI_IMAGE_TAG'] = os.environ['CI_COMMIT_SHA'][:8]
